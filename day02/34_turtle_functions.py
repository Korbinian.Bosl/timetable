from math import sin, cos
import turtle as t

t.speed(0)  # adjust speed of turtle, 0 is fastest, 1 is slowest
t.hideturtle()

# single shape
def square(sidelength):
    for i in range(4):
        t.forward(sidelength)
        t.right(90)


def star(num_points, sidelength):
    ext_angle = 180 - (180 / num_points)
    for i in range(num_points + 1):
        t.forward(sidelength)
        t.right(ext_angle)


def triangle(sidelength):
    for i in range(3):
        t.forward(sidelength)
        t.left(120)


# chained function to draw many shapes
def many_squares(num_of_shapes):
    for i in range(num_of_shapes):
        t.setheading(5 * i)  # rotate by 5 degrees further with each iteration
        square(80)  # draw square in rotated position


def spiral_squares(num_of_shapes, sidelength):
    for i in range(num_of_shapes):
        t.setx(0)
        t.sety(0)
        t.setheading(5 * i)  # rotate by 5 degrees further with each iteration
        square(sidelength)  # draw square in rotated position
        sidelength += 5


def spiral_stars(num_of_shapes, sidelength):
    t.color("violet", "light blue")
    t.begin_fill()
    for i in range(num_of_shapes):
        t.setx(0)
        t.sety(0)
        t.setheading(5 * i)
        star(5, sidelength)
        sidelength += 5
    t.end_fill()


# draw more complex shape
def trefoil():  # amplitude, angular displacement
    t.screensize(400, 400, "#005744")  # set background color
    t.pensize(3)  # set width of pen stroke

    # experiment with changing these variables - how does the drawing change?
    s = 0
    a = 80
    k = 3
    r = 40
    linework = "#019879"

    # draw expanding trefoil
    t.penup()
    for i in range(0, 1500):
        t.color(linework)
        s += 0.01
        a += 0.05

        x = a * cos(k * s) * cos(s)
        y = a * cos(k * s) * sin(s)

        t.goto(x, y)
        t.pendown()

    # draw circle
    t.penup()
    for i in range(0, 620):
        t.color(linework)
        s += 0.01
        x = r * cos(s)
        y = r * sin(s)

        t.goto(x, y)
        t.pendown()


#### CALL FUNCTION ###

# many_squares(60)
# spiral_stars(60, 5)
#trefoil()

square(100)
square(20)
square(5)


t.done()
