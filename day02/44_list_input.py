# ask for several numbers
# store in list
# find maximum

def numbers_from_input():
    """Ask user for input to build a list of positive integers."""
    nums = []
    while True:
        n = input("A positive number (q to stop): ")
        if n == "q":
            break
        n = int(n)
        if n < 0:
            print("That was negative. Try again.")
            continue
        nums.append(n)
    return nums


def my_max(nums):
    """Find the maximum of a list of numbers."""
    maxnum = nums[0]
    for num in nums[1:]:
        if num > maxnum:
            maxnum = num
    return maxnum


nums = numbers_from_input()
print(f"You gave me {len(nums)} numbers.")
print("Your numbers are:", nums)

print("The maximum number is:", max(nums))

print("Doing max by hand...")
maxnum = my_max(nums)
print("My own max is:", maxnum)
