a = 5
b = 9

#############

if a > b:
    print("Bigger!")

print("BBB")


#############

if a == b:
    print("The same!")
    print("More than one line can be here:")
    print("as")
    print("many")
    print("as")
    print("you")
    print("like.")

print("CCC")

#############

if a > 5 and b < 10:
    print("A is above 5 and B is less than 10")
