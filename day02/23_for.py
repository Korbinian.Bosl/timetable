for letter in "Hello":
    print(letter)

print("=" * 25)

for mitt_langt_variabelnavn_som_jeg_kan_velge_fritt in "Hello":
    print(mitt_langt_variabelnavn_som_jeg_kan_velge_fritt)
    
print("=" * 25)

x = "Hello"
for letter in x:
    print(letter)
    
print("=" * 25)

print("Range 1:")
for n in range(7):
    print(n)

print("Range 2:")
for n in range(3, 7):
    print(n)

print("Range 3:")
for n in range(2, 17, 3):
    print(n)

#########################
# 2 ways to do the same
#########################

print("Afternoon 1:")
result = ""
for letter in "Good afternoon":
    if letter == "o":
        result += "0"
    else:
        result += letter
print(result)

print("Afternoon 2:")
result = ""
for letter in "Good afternoon":
    if letter == "o":
        result += "0"
        continue
    result += letter
print(result)
