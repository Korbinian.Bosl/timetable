# Functions can also be used to keep related instructions together
# allowing them to be reused consistently, e.g. to make "drawings"
def ascii_wave():
    """Print an ascii wave"""
    print("\\" * 40)
    print("/" * 40)
    print("\\" * 40)


def ascii_cat():
    """Print an ascii cat"""
    print(" /\_/\\")
    print("( o.o )")
    print(" > ^ <")


def ascii_cube():
    """Print an ascii cube"""
    print("+------+.")
    print("|`.    | `.")
    print("|  `+--+---+")
    print("|   |  |   |")
    print("+---+--+.  |")
    print(" `. |    `.|")
    print("   `+------+")


def ascii_castle():
    """Print an ascii castle"""
    print(" _   |~  _")
    print("[_]--'--[_]")
    print('|\'|""`""|\'|')
    print("| | /^\\ | |")
    print("|_|_|I|_|_|")


print()
print("Here is a cat:")
ascii_cat()

print()
print("Here are two cats:")
ascii_cat()
ascii_cat()

print()
print("We can also print a castle:")
ascii_castle()


print()
print("Or a cat on a wave:")
ascii_cat()
ascii_wave()
