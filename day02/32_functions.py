print("--------------------------------------")


def hello(name):
    """Greet a person nicely"""
    text = f"Hello, {name}!"
    return text


print("Print result of hello function with Alice as input:")
print(hello("Alice"))

print("--------------------------------------")

# can you draw the program flow for this function?
def order(food, number):
    """Make an order of one type of food"""
    if number == 1:
        return f"{number} {food}"
    else:
        return f"{number} {food}s"


print("Print result of order function with food: coffee and number: 1 as inputs:")
print(order("coffee", 1))
print("--------------------------------------")
print("Print result of order function with food: coffee and number: 3 as inputs:")
print(order("coffee", 3))

print("--------------------------------------")


def make_order(name, coffee_number, cookie_number):
    """Make an order of coffee and cookies"""
    greeting = hello(name)
    coffee_order = order("coffee", coffee_number)
    cookie_order = order("cookie", cookie_number)

    return f"{greeting}\nCan I have {coffee_order} and {cookie_order}, please?"


print("Lets call make_order(2, 10) to order 2 coffees and 10 cookies from Alice:")
print(make_order("Alice", 2, 10))
