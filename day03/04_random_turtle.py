import turtle as t
import random
import math
from statistics import mean

t.setup(1000, 1000)  # set canvas size
t.hideturtle()

steps = 100
step_size = 10
distances = []

# create 5 turtles
a = t.Turtle()
b = t.Turtle()
c = t.Turtle()
d = t.Turtle()
e = t.Turtle()

# assign different color to each turtle
a.color("red")
b.color("green")
c.color("blue")
d.color("orange")
e.color("black")

turtles = [a, b, c, d, e]

for tu in turtles:
    # instructions for walk path
    tu.goto(0, 0)
    tu.speed(0)
    tu.down()
    for _ in range(steps):
        direction = random.choice([0, 90, 180, 270]) # grid walk
        # direction = random.uniform(0,360) # any direction allowed
        tu.setheading(direction) 
        tu.forward(step_size)

    tu.up()

    # distance traveled is the square root of the sum of squared change in x and squared change in y
    xdist = tu.xcor()
    ydist = tu.ycor()
    d = math.sqrt(xdist**2 + ydist**2)
    distances.append(d)

print(distances)
print(f"Direct way to calculate average distance yields {mean(distances):.2f}")

# check against theory (holds true for many repetitions of random walk, fewer repetitions is less accurate)
avg_distance = step_size * math.sqrt(steps)
print(f"Theoretical calc for avg distance is {avg_distance:.2f}")

t.done()
