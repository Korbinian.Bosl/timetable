2023 Collaborative programming school
=====================================

In this repository, we'll include all relevant material, it will be updated frequently during the school.

Timetable
---------

This is a rough day-by-day plan, but we may switch up some items at short notice.

Other than the first and last days, doors will open at 8:45 so we can start on time at 9:00. Lunch is at 12:00 each day, and we finish around 16:30. 

----

Day 1 - Monday 19 June
......................

* from 10:00 - Registration
* 10:30 - Welcome and introduction / 
* Installation of tools: https://inf100.ii.uib.no/notat/installere/
* Windows users also: https://gitforwindows.org/
* 13:00 - Introduction to the command line: https://swcarpentry.github.io/shell-novice/



Day 2 - Tuesday 20 June
.......................

* 09:00 - Python intro (`Slides <day02/2023-collab-Py-intro.pdf>`_)
* 13:00 - Real-life examples: Jill Walker Rettberg - Machine Vision & GPT stories
* 13:30 - Version control with Git (part 1: individual use)

Day 3 - Wednesday 21 June
.........................

* 09:00 - Program design, objects / functional styles
* 11:15 - Copyright and licensing
* 13:00 - Real-life examples: Ali Farnudi - Wobbly membranes
* 13:30 - Clean code, Exercises: objects / 'fetch -> analyse -> present' workflow with various datasets
* 18:00 - Dinner at Dr. Wiesener

Day 4 - Thursday 22 June
........................

* 09:00 - Visualisation: background and python tools
* 13:00 - Real-life examples: Rune Kyrkjebø - CLARIN
* 13:30 - Exercises, including git part 2: branches / merging

Day 5 - Friday 23 June
......................

* 09:00 - Useful libraries - finding and using them
* 10:30 - Testing / debugging / (profiling)
* 13:00 - Real-life examples: Henrik Askjer / Jan Ole Bangen - The Language Collections
* 13:30 - Git part 3 - conversational development + exercises

----

Day 6 - Monday 26 June
......................

* 09:00 - Team collaboration - part 1
* 10:00 - Presentation of project tasks, choose project
* 10:45 - Team collaboration - part 2
* 13:00 - Start project work

Day 7 - Tuesday 27 June
.......................

* 09:00 - Documentation tools
* 10:45 - Open data, FAIR
* 13:00 - Project work

Day 8 - Wednesday 28 June
.........................

* 09:00 - Mixing languages
* 13:00 - Project work

Day 9 - Thursday 29 June
........................

* 09:00 - BUFFER
* 13:00 - Project work

Day 10 - Friday 30 June
.......................

* 09:00 - Finalise presentations
* 09:45 - Present results
* 12:30 - End
